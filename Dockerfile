FROM ruby:3.1.2

ENV PROJECT_ROOT /usr/src/app

RUN apt-get update -qq && \
  apt-get install -y -qq \
  build-essential

WORKDIR $PROJECT_ROOT

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock

RUN bundle install

COPY . .
